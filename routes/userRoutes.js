const express = require('express');
const router = express.Router();

const userControllers = require('../controllers/userControllers');

router.post('/', userControllers.createUserController);
router.get('/', userControllers.getAllUsersController);

// updating user's username route
router.put('/:id', userControllers.updateUserNameController);

// retrieving single user route
router.get('/getSingleUser/:id', userControllers.getSingleUserController);

module.exports = router;